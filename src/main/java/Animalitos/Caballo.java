/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Animalitos;

/**
 *
 * @author Mario
 */
public class Caballo {
    
    private int identificador;
    private String nombre;
    private String color;
    private String genero;
    
    public Caballo() {

}
 public Caballo(int identificador, String nombre, String color, String genero) {    

     this.identificador = identificador;
     this.nombre = nombre;
     this.color = color;
     this.genero = genero;
 }

    public int getIdentificador() {
        return identificador;
    }

    public void setIdentificador(int identificador) {
        this.identificador = identificador;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getGenero() {
        return genero;
    }

    public void setGenero(String genero) {
        this.genero = genero;
    }



}
