/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Back;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 *
 * @author Mario
 */
public class Bdd {
 
    private String bdd = "bddgranja";
    private String user = "root";
    private String password = "";
    private String url = "jdbc:mysql://localhost:3306/"+bdd;
    private Connection connection = null;
    
    public Connection getConexion(){
        
    try{
    connection = (Connection)DriverManager.getConnection(this.url, this.user, this.password);
        
    }catch (SQLException e){
        System.out.println(e);
    }
    
    return connection;
            
    }
    
}
