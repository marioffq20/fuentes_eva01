/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Back;

import Animalitos.Caballo;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 *
 * @author Mario
 */
public class AdminCaballos {
    
   Bdd conexion = new Bdd();
   
   Connection c;
   PreparedStatement ps;
   ResultSet rs;
    
    public boolean consultarCaballos(int identificador){
       String sql = "SELECT * FROM animalitos WHERE identificador="+"'"+identificador+"'";
               
    try{
        c = conexion.getConexion();
        ps = c.prepareStatement(sql);
        rs = ps.executeQuery();
        
        if(rs.next()){
            
            System.out.println("El nombre del caballo es: "+rs.getString("nombre"));
            System.out.println("El color del caballo es: "+rs.getString("color"));
            System.out.println("El genero del caballo es: "+rs.getString("genero"));
        }else{
            return false;
        
        }
    }catch (SQLException e){
        e.printStackTrace();
        System.out.println("Error en la informacion del caballo");
    
    }
    return true;
}
 
    public boolean agregarCaballos (Caballo caballo){
        
        String sql = "INSERT INTO animalitos (identificador, nombre, color, genero) VALUES(?, ?, ?, ?)";
        
            try{
            c = conexion.getConexion();
            ps = c.prepareStatement(sql);
         
        ps.setInt(1, caballo.getIdentificador());    
        ps.setString(2, caballo.getNombre());
        ps.setString(3, caballo.getColor());
        ps.setString(4, caballo.getGenero());
        
        ps.executeUpdate();
        
    }catch (SQLException e){
        e.printStackTrace();
        System.out.println("Error al agregar info del caballo");
    
    }
            return true;
    }
    public boolean eliminarCaballos(int identificador){
        
        String sql = "DELETE FROM animalitos WHERE identificador="+"'"+identificador+"'";
        
        try{
            c = conexion.getConexion();
            ps = c.prepareStatement(sql);
         
            ps.executeUpdate();
        
    }catch (SQLException e){
        e.printStackTrace();
        System.out.println("Error al eliminar info del caballo");
    
    }
        return true;
    }
}

