/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Sistema;

import Animalitos.Caballo;
import Back.AdminCaballos;
import java.util.Scanner;

/**
 *
 * @author Mario
 */
public class Menu {
    
    static AdminCaballos admincaballos = new AdminCaballos();
    
    public static void main(String[] args) {
        
      Scanner teclado = new Scanner(System.in);
     
      int opcion;
      int respuesta;
      
      do{
          System.out.println("Bienvenido a la granja de caballos");
          System.out.println("Ingrese una opción");
          System.out.println("1.- Agregar un caballo");
          System.out.println("2.- Consultar informacion");
          System.out.println("3.- Eliminar un caballo");
          System.out.println("4.- Salir");
          opcion = teclado.nextInt();
          System.out.println("");
          
          switch(opcion){
          
          case 1:
          
          do{
          Caballo caballoballo = new Caballo();
          
              System.out.println("Agregando caballo al sistema");
              System.out.println("Ingrese el número identificador del caballo");
              int identificador=teclado.nextInt();
              
              System.out.println("Ingrese el nombre del caballo");
              String nombre=teclado.next();
              
              System.out.println("Ingrese el color del caballo");
              String color=teclado.next();
              
              System.out.println("Ingrese el genero del caballo");
              String genero=teclado.next();
              
              caballoballo.setIdentificador(identificador);
              caballoballo.setNombre(nombre);
              caballoballo.setColor(color);
              caballoballo.setGenero(genero);
              
              
              boolean resp = admincaballos.agregarCaballos(caballoballo);
              if(resp==true){
                   System.out.println("Informacion ingresada exitosamente");
              }else{
                  System.out.println("Error al ingresar los datos");
              }
             
              
              System.out.println("¿Desea agregar otro caballo? 1.SI  2.NO");
         
              respuesta=teclado.nextInt();
              
          }while(respuesta==1);
          
        System.out.println("");
          System.out.println("¿Volver al menu? 1.SI  2.NO");
          opcion=teclado.nextInt();
          
          
          break;
              
          case 2:
              
              
              System.out.println("Ingrese el número identificador del caballo");
              int identificador_caballo=teclado.nextInt();
              
              boolean resp_consultar = admincaballos.consultarCaballos(identificador_caballo);
              
              if(resp_consultar==false){
                  System.out.println("Caballo no encontrado");
              }
               
          System.out.println("");
          System.out.println("¿Volver al menu? 1.SI  2.NO");
          opcion=teclado.nextInt();
              
              break;
               
               
          case 3:
              do{
                  boolean resp_eliminacion = false;
                  System.out.println("Ingrese el numero identificador del caballo para eliminarlo");
                  int eliminar_caballos=teclado.nextInt();
                  
                  System.out.println("");
                  System.out.println("Informacion encontrada: ");
                  
                  boolean resp_eliminar = admincaballos.consultarCaballos(eliminar_caballos);
                  
                  if(resp_eliminar==false){
                      System.out.println("Caballo no encontrado");
                  }else{
                      System.out.println("¿Seguro que desea eliminar los datos del caballo?  1.SI  2.NO");
                      int confirmacion=teclado.nextInt();
                      resp_eliminacion = admincaballos.eliminarCaballos(eliminar_caballos);
                      
                  }
                  
                  if(resp_eliminacion==true){
                      System.out.println("Caballo eliminado exitosamente");
                  }
                  
                  System.out.println("¿Eliminar otra informacion de caballos? 1.SI  2.NO" );
                  respuesta=teclado.nextInt();
              }while(respuesta==1);
              
          System.out.println("");
          System.out.println("¿Volver al menu? 1.SI  2.NO");
          opcion=teclado.nextInt();
              
              
              
           break;
           
          case 4:
              
              
              System.out.println("¿Quiere abandonar el programa?  1.SI  2.NO");

                    opcion = teclado.nextInt();

                    if(opcion == 1){
                        opcion = 2;
                    }else{
                        opcion = 1;
                    }
                    break;
                    
              
           
            
          default:
              System.out.println("OPCION INVALIDA");
              
          }
      }while(opcion==1);
          
          
    }
}

      


